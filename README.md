# COFFEA

![Image](documentation/specimen/SpecimenCoffeaCanephora13.png)

DEUTSCH
Die Typefamily Coffea im Stil Canephora wurde inspiriert durch das Lebenselixier Kaffee. Die Anatomie der Buchstaben leitet sich in Form und Größe der der Kaffeebohnen ab. Die Sorte Canephora (oder auch Robusta genannt) hat im Vergleich zur Arabica sehr kleine Bohnen. Daher resultiert eine recht niedrige x-Höhe der Kleinbuchstaben. Geschmacklich verfügt die Canephora-Bohne nur über die Hälfte der geschmachstragenden Gene, die eine Aralica-Bohne aufweist, nämlich nur 22 statt 44. Reiner Canephora-Kaffee schmeckt daher nicht ganz so ausgewogen, er hat geschmacklich „Ecken und Kanten“, meist ist er sehr intensiv. Diese Tatsache verleiht der Schriftart ihr kantiges Erscheinungsbild.
Die Familie kommt in 8 Schnitten, die in ihren Dicken von den gängigsten Röstgraden abgeleitet sind.

ENGLISCH
The Typefamily Coffea in Canephora style was inspired by the elixir of life, coffee. The anatomy of the letters is based on the shape and size of the coffee beans. The Canephora (or Robusta) variety has very small beans compared to the Arabica. This results in a rather low x-height of the lowercase letters. In terms of taste, the Canephora bean has only half of the taste-bearing genes found in an Aralica bean, i.e. only 22 instead of 44. Pure Canephora coffee therefore does not taste quite as balanced, it has "rough edges" in terms of taste and is usually very intense. This fact gives the font its sharp-edged appearance.
The family comes in 8 cuts, whose thicknesses are inspired by the most common roasting degrees.

Now available on the NoFoundry Map.
Have fun with it!

## About the author

Cara Kollmann is a student at HfG Karlsruhe.

## Contact

cfkollmann@hfg-karlsruhe.de

## License

This Font Software is licensed under the NoFoundry Eula. This license is enclosed in this repository. For any questions, feel free to contact the [NoFoundry ](abc@nofoundry.xyz).

## Repository Layout

This font repository structure is inspired by [Unified Font Repository v0.3](https://github.com/unified-font-repository/Unified-Font-Repository).

## Publishing

This font is made available through the [NoFoundry](http://nofoundry.xyz/).
